
# EZ-FLASH OMEGA Kernel - MEdition

## What is this?
This is a fork of the [EZ-Flash Omega kernel](https://github.com/ez-flash/omega-kernel). I aim to improve it, by adding features, fixing broken things and making the code cleaner and easier to read for everyone.

## How to use it?

### Using a pre-compiled binary from the Downloads tab

Just download the release you want to use, put the `ezkernel.bin` file at the root of your SD card and hold R while booting the EZ-Flash Omega. The new kernel will be installed.

### By building it from source

Install [DevKitARM](https://devkitpro.org/wiki/Getting_Started), `cd` into the repository where you extracted the sources in and run `make`. If all went well, a binary file named `ezkernel.bin` has been created. Just follow the same installation procedure as above.
