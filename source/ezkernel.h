#ifndef	EZKERNEL_H
#define EZKERNEL_H

#include "ff13c/ff.h"

#define MAX_pReadCache_size 0x20000
#define MAX_files     0x200
#define MAX_folder    0x100
#define MAX_NOR				0x40

#define MAX_path_len 0x100

#define FAT_table_size 0x400
#define FAT_table_SAV_offset 0x200
#define FAT_table_RTS_offset 0x300

#define DEBUG

#define VideoBuffer    (u16*)0x6000000
#define Vcache         (u16*)pReadCache
#define RGB(r,g,b) ((r)+(g<<5)+(b<<10))

#define PSRAMBase_S98	    (u32) 0x08800000
#define FlashBase_S98       (u32) 0x09000000
#define FlashBase_S98_end	(u32) 0x09800000

#define SAVE_sram_base (u32)0x0E000000
#define SRAMSaver		   (u32)0x0E000000

#define UNBCD(x) (((x) & 0xF) + (((x) >> 4) * 10))
#define _BCD(x) ((((x) / 10)<<4) + ((x) % 10))
#define _YEAR	0
#define _MONTH	1
#define _DAY	2
#define _WKD	3
#define _HOUR	4
#define _MIN	5
#define _SEC	6

typedef struct FM_NOR_FILE_SECT { ////save to nor
        char filename[100];
        u16 rompage;
        u16 have_patch;
        u16 have_RTS;
        u16 reserved;
        u32 filesize;
        u32 reserved2;
        char gamename[0x10];
} FM_NOR_FS;

typedef struct FM_Folder_SECT {
        char filename[100];
} FM_Folder_FS;

typedef struct FM_FILE_SECT {
        char filename[100];
        u32 filesize;
} FM_FILE_FS;

typedef enum {
    SD_list = 0, NOR_list = 1, SET_win = 2, HELP = 3,
} PAGE_NUM;

FM_NOR_FS pNorFS[MAX_NOR];
char pReadCache [MAX_pReadCache_size];
char GAMECODE[4];

u32 FAT_table_buffer[FAT_table_size / 4];
FIL gfile;

u32 game_total_NOR;

u16 gl_engine_sel;

u16 gl_show_Thumbnail;

u16 gl_select_lang;

u16 gl_reset_on;
u16 gl_rts_on;
u16 gl_sleep_on;
u16 gl_cheat_on;

u16 gl_color_selected;
u16 gl_color_text;
u16 gl_color_selectBG_sd;
u16 gl_color_selectBG_nor;
u16 gl_color_MENU_btn;
u16 gl_color_cheat_count;
u16 gl_color_cheat_black;
u16 gl_color_NORFULL;
u16 gl_color_btn_clean;

u16 gl_ingame_RTC_open_status;

u32 gl_currentpage;
u32 gl_norOffset;

u32 Setting_window(void);

u32 LoadRTSfile(TCHAR *filename);
void ShowTime(u32 page_num, u32 page_mode);

void delay(u32 R0);

void CheckSwitch();
void CheckLanguage();

u32 Check_game_save_FAT(TCHAR *filename, u32 game_save_rts);

#endif
