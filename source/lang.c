#include "lang.h"

// English
const char en_init_error[] = "Micro SD card init. error";
const char en_power_off[] = "Power off";
const char en_init_ok[] = "Micro SD card init. OK";
const char en_Loading[] = "Loading...";
const char en_file_overflow[] = "The file overflowed";

const char en_menu_btn[] = "[B]Cancel  [A]Ok";
const char en_writing[] = "Writing...";
const char en_lastest_game[] = "Select the latest";

const char en_time[] = "     Time";
const char en_Mon[] = "Mon";
const char en_Tues[] = "Tue";
const char en_Wed[] = "Wed";
const char en_Thur[] = "Thu";
const char en_Fri[] = "Fri";
const char en_Sat[] = "Sat";
const char en_Sun[] = "Sun";

const char en_addon[] = "    Addon";
const char en_reset[] = "Reset";
const char en_rts[] = "Savestate";
const char en_sleep[] = "Sleep";
const char en_cheat[] = "Cheat";

const char en_hot_key[] = "Sleep key";
const char en_hot_key2[] = " Menu key";

const char en_language[] = " Language";
const char en_lang[] = "English";
const char zh_lang[] = "";
const char en_set_btn[] = "Set";
const char en_ok_btn[] = " Ok";

const char en_formatnor_info[] = "Sure? ETA ~4 mins";

const char en_check_sav[] = "Checking sav file";
const char en_make_sav[] = "Creating sav file";

const char en_check_RTS[] = "Checking rts file";
const char en_make_RTS[] = "Creating rts file";

const char en_check_pat[] = "Checking pat file";
const char en_make_pat[] = "Creating pat file";

const char en_generating_emu[] = "Generating emu...";
const char en_loading_game[] = "Loading game...";

const char en_engine[] = "   Engine";
const char en_use_engine[] = "Fast patch engine";

const char en_recently_play[] = "Recently played";

const char en_START_help[] = "Open recently played list";
const char en_SELECT_help[] = "Thumbnail toggle";
const char en_L_A_help[] = "Multiboot";
const char en_LSTART_help[] = "Delete file";
const char en_online_manual[] = "Online manual";

const char en_no_game_played[] = "No game played yet";

const char en_ingameRTC[] = " Game RTC";
const char en_ingameRTC_open[] = "On";
const char en_ingameRTC_close[] = "Off"; //TURNOFF TO POWER SAVE

const char en_error_0[] = "Folder error";
const char en_error_1[] = "File error";
const char en_error_2[] = "SAVER error";
const char en_error_3[] = "Save error";
const char en_error_4[] = "Read save error";
const char en_error_5[] = "Make save error";
const char en_error_6[] = "RTS file error";

const char en_credit_0[] = "EZFO Kernel - MEdition";
const char en_credit_1[] = "2019, M4x1m3";

const char *en_rom_menu[] = {
        "Clean boot", "Boot w/ addons", "NOR Write (clean)",
        "NOR Write (addons)", "Save type", "Cheat" };
const char *en_nor_op[3] = {
        "Direct boot", "Delete", "Format" };

void LoadEnglish(void) {
    gl_init_error = (char*) en_init_error;
    gl_power_off = (char*) en_power_off;
    gl_init_ok = (char*) en_init_ok;
    gl_Loading = (char*) en_Loading;
    gl_file_overflow = (char*) en_file_overflow;

    gl_menu_btn = (char*) en_menu_btn;
    gl_writing = (char*) en_writing;
    gl_lastest_game = (char*) en_lastest_game;

    gl_time = (char*) en_time;
    gl_Mon = (char*) en_Mon;
    gl_Tues = (char*) en_Tues;
    gl_Wed = (char*) en_Wed;
    gl_Thur = (char*) en_Thur;
    gl_Fri = (char*) en_Fri;
    gl_Sat = (char*) en_Sat;
    gl_Sun = (char*) en_Sun;
    gl_addon = (char*) en_addon;
    gl_reset = (char*) en_reset;
    gl_rts = (char*) en_rts;
    gl_sleep = (char*) en_sleep;
    gl_cheat = (char*) en_cheat;

    gl_hot_key = (char*) en_hot_key;
    gl_hot_key2 = (char*) en_hot_key2;

    gl_language = (char*) en_language;
    gl_en_lang = (char*) en_lang;
    gl_zh_lang = (char*) zh_lang;
    ;
    gl_set_btn = (char*) en_set_btn;
    gl_ok_btn = (char*) en_ok_btn;
    gl_formatnor_info = (char*) en_formatnor_info;

    gl_check_sav = (char*) en_check_sav;
    gl_make_sav = (char*) en_make_sav;

    gl_check_RTS = (char*) en_check_RTS;
    gl_make_RTS = (char*) en_make_RTS;

    gl_check_pat = (char*) en_check_pat;
    gl_make_pat = (char*) en_make_pat;

    gl_generating_emu = (char*) en_generating_emu;
    gl_loading_game = (char*) en_loading_game;

    gl_engine = (char*) en_engine;
    gl_use_engine = (char*) en_use_engine;

    gl_recently_play = (char*) en_recently_play;

    gl_START_help = (char*) en_START_help;
    gl_SELECT_help = (char*) en_SELECT_help;
    gl_L_A_help = (char*) en_L_A_help;
    gl_LSTART_help = (char*) en_LSTART_help;
    gl_online_manual = (char*) en_online_manual;

    gl_no_game_played = (char*) en_no_game_played;

    gl_ingameRTC = (char*) en_ingameRTC;
    //gl_offRTC_powersave = (char*)en_offRTC_powersave;
    gl_ingameRTC_open = (char*) en_ingameRTC_open;
    gl_ingameRTC_close = (char*) en_ingameRTC_close;

    gl_error_0 = (char*) en_error_0;
    gl_error_1 = (char*) en_error_1;
    gl_error_2 = (char*) en_error_2;
    gl_error_3 = (char*) en_error_3;
    gl_error_4 = (char*) en_error_4;
    gl_error_5 = (char*) en_error_5;
    gl_error_6 = (char*) en_error_6;

    gl_credit_0 = (char*) en_credit_0;
    gl_credit_1 = (char*) en_credit_1;

    gl_rom_menu = (char**) en_rom_menu;
    gl_nor_op = (char**) en_nor_op;
}
