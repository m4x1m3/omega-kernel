#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <gba_base.h>
#include <gba_dma.h>

#include "ezkernel.h"
#include "font.h"

int current_y = 1;

void IWRAM_CODE Clear(u16 x, u16 y, u16 w, u16 h, u16 c, u8 isDrawDirect) {
    u16 *p;
    u16 yi, ww, hh;

    if (isDrawDirect)
        p = VideoBuffer;
    else
        p = Vcache;

    hh = (y + h > 160) ? 160 : (y + h);
    ww = (x + w > 240) ? (240 - x) : w;

    //u16 tmp[240];
    for (u32 i = 0; i < 240; i++)
        ((u16*) pReadCache)[i] = c;

    for (yi = y; yi < hh; yi++)
        dmaCopy(pReadCache, p + yi * 240 + x, ww * 2);
}

void IWRAM_CODE ClearWithBG(u16* pbg, u16 x, u16 y, u16 w, u16 h, u8 isDrawDirect) {
    u16 *p;
    u16 yi, ww, hh;

    if (isDrawDirect)
        p = VideoBuffer;
    else
        p = Vcache;

    hh = (y + h > 160) ? 160 : (y + h);
    ww = (x + w > 240) ? (240 - x) : w;

    for (yi = y; yi < hh; yi++)
        dmaCopy(pbg + yi * 240 + x, p + yi * 240 + x, ww * 2);
}

void IWRAM_CODE DrawPic(u16 *GFX, u16 x, u16 y, u16 w, u16 h, u8 isTrans, u16 tcolor, u8 isDrawDirect) {
    u16 *p, c;
    u16 xi, yi, ww, hh;

    if (isDrawDirect)
        p = VideoBuffer;
    else
        p = Vcache;

    hh = (y + h > 160) ? 160 : (y + h);
    ww = (x + w > 240) ? (240 - x) : w;

    if (isTrans) {
        for (yi = y; yi < hh; yi++)
            for (xi = x; xi < x + ww; xi++) {
                c = GFX[(yi - y) * w + (xi - x)];
                if (c != tcolor)
                    p[yi * 240 + xi] = c;
            }
    } else {
        for (yi = y; yi < hh; yi++)
            dmaCopy(GFX + (yi - y) * w, p + yi * 240 + x, w * 2);
    }
}

int binary_search(const unsigned short* sorted_list, int low, int high, short element) {
    int middle;
    while (low <= high)  {
        middle = low + (high - low)/2;
        if (element > sorted_list[middle])
            low = middle + 1;
        else if (element < sorted_list[middle])
            high = middle - 1;
        else
            return middle;
    }
    return -1;
}

int count_utf8_code_points(const char *s) {
    int count = 0;
    while (*s) {
        count += (*s++ & 0xC0) != 0x80;
    }
    return count;
}

int utf8_simple(char* s, u32* c) {
    int read;
    if (s[0] < 0x80) {
        *c = s[0];
        read = 1;
    } else if ((s[0] & 0xe0) == 0xc0) {
        *c = ((u32)(s[0] & 0x1f) <<  6) |
             ((u32)(s[1] & 0x3f) <<  0);
        read = 2;
    } else if ((s[0] & 0xf0) == 0xe0) {
        *c = ((u32)(s[0] & 0x0f) << 12) |
             ((u32)(s[1] & 0x3f) <<  6) |
             ((u32)(s[2] & 0x3f) <<  0);
        read = 3;
    } else if ((s[0] & 0xf8) == 0xf0 && (s[0] <= 0xf4)) {
        *c = ((u32)(s[0] & 0x07) << 18) |
             ((u32)(s[1] & 0x3f) << 12) |
             ((u32)(s[2] & 0x3f) <<  6) |
             ((u32)(s[3] & 0x3f) <<  0);
        read = 4;
    } else {
        *c = 0; // invalid
        read = 1; // skip this byte
    }
    if (*c >= 0xd800 && *c <= 0xdfff)
        *c = -1; // surrogate half
    return read;
}



void DrawHZText12(char *str, u16 len, u16 x, u16 y, u16 c, u8 isDrawDirect) {
    u32 i, l, hi = 0;
    u32 location;
    u8 cc;
	
	u32 c1;
	
    u16 *v;
    u16 *p1 = Vcache;
    u16 *p2 = VideoBuffer;
    u16 yy;

    if (isDrawDirect)
        v = p2;
    else
        v = p1;

    if (len == 0)
        l = count_utf8_code_points(str);
    else if (len > count_utf8_code_points(str))
        l = count_utf8_code_points(str);
    else
        l = len;

    if ((u16) (len * 6) > (u16) (240 - x))
        len = (240 - x) / 6;
    while (hi < l) {
        hi += utf8_simple(str + hi, &c1);
		if (c1 & 0xFFFF0000) {
			c1 = 0;
		}
		
		yy = 240 * y;
		location = binary_search(font.Index, 0, font.Chars, (u16) c1) * font.Height;
		
		if (location == -1) {
			location = 0;
		}
		
		for (i = 0; i < font.Height; i++) {
			cc = font.Bitmap[location + i];
			if (cc & 0x01)
				v[x + 7 + yy] = c;
			if (cc & 0x02)
				v[x + 6 + yy] = c;
			if (cc & 0x04)
				v[x + 5 + yy] = c;
			if (cc & 0x08)
				v[x + 4 + yy] = c;
			if (cc & 0x10)
				v[x + 3 + yy] = c;
			if (cc & 0x20)
				v[x + 2 + yy] = c;
			if (cc & 0x40)
				v[x + 1 + yy] = c;
			if (cc & 0x80)
				v[x + yy] = c;
			yy += 240;
		}
		x += font.Width;
    }
}

void ShowbootProgress(char *str) {
    u8 str_len = strlen(str);
    Clear(60, 160 - 15, 120, 15, gl_color_cheat_black, 1);
    DrawHZText12(str, 0, (240 - str_len * 6) / 2, 160 - 15, gl_color_text, 1);
}
