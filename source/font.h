#ifndef FONT_H
#define FONT_H

// (c) 2009, 2010 Lutz Sammer, License: AGPLv3

	/// bitmap font structure
struct bitmap_font {
	unsigned char Width;		///< max. character width
	unsigned char Height;		///< character height
	unsigned short Chars;		///< number of characters in font
	const unsigned char *Widths;	///< width of each character
	const unsigned short *Index;	///< encoding to character index
	const unsigned char *Bitmap;	///< bitmap of all characters
};

const struct bitmap_font font;

#endif
